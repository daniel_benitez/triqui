import random

class Tablero:
    
    def __init__(self):
        #Matriz de 3x3 para simular el tablero del Triqui
        self.casillas = [['','',''], ['','',''], ['','','']]
        self.movimientos = 0
        self.turno = 'x'
        
    def verificar_triqui(self):
        pass
    
    def verificar_empate(self):
        pass
    
    def cambiar_turno(self):
        pass
    
    def seleccionar_jugador(self, miJugador1, miJugador2):
        x = random.randint(1,2)
        if x == 1:
            miJugador1.simbolo = 'x'
            miJugador2.simbolo = 'o'
        else:
            miJugador1.simbolo = 'o'
            miJugador2.simbolo = 'x'
            
    def limpiar_tablero(self):
        self.casillas = [['','',''], ['','',''], ['','','']]
        
    def iniciar_juego(self):
        pass
    
    def finalizar_juego(self):
        pass